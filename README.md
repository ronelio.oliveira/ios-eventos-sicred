# Desafio programação iOS

Este projeto é o resultado do trabalho desenvolvido por Ronélio Oliveira, para [Teste para Desenvolvedor(a) iOS do Woop Sicredi](https://github.com/WoopSicredi/jobs/issues/3)

# Requisitos

 - iOS 13 ou superior
 - Xcode 12.4 ou superior

# Bibliotecas de terceiros
Para auxiliar no desenvolvimento de algumas rotinas, algumas bibliotecas foram utilizadas:

 - SDWebImageSwiftUI para renderização e cache de imagens http/https [ver no GitHub](https://github.com/SDWebImage/SDWebImageSwiftUI)
 - Alamofire para requisições http/https [ver no GitHub](https://github.com/Alamofire/Alamofire)

## Outras informações

 - SwiftPackage Manager para gestão de dependências
 - SwiftUI para composição das telas e navegação

 ## Preview
 ![Eventos](demo/demo.gif)