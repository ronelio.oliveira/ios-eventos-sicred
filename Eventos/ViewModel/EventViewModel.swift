//
//  EventViewModel.swift
//  Eventos
//
//  Created by ronelio on 30/03/21.
//

import Foundation
import Combine

class EventViewModel: ObservableObject {
    private let url = "https://5f5a8f24d44d640016169133.mockapi.io/api/events"
    
    private var task: AnyCancellable?
    @Published var events: [Event] = []

    func fetchEvents() {
        task = URLSession.shared.dataTaskPublisher(for: URL(string: url)!)
          .map { $0.data }
          .decode(type: [Event].self, decoder: JSONDecoder())
          .replaceError(with: [])
          .eraseToAnyPublisher()
          .receive(on: RunLoop.main)
          .assign(to: \EventViewModel.events, on: self)
    }
}

