//
//  EventCheckinViewModel.swift
//  Eventos
//
//  Created by ronelio on 31/03/21.
//

import Foundation
import Combine
import Alamofire
import SwiftUI

class EventCheckinViewModel: ObservableObject {
    private let url = "https://5f5a8f24d44d640016169133.mockapi.io/api/checkin"
    private var task: AnyCancellable?
    @Published var eventCheckin: EventCheckin = EventCheckin.init(eventId: "", name: "", email: "")
    @Published var falha = false
    @Published var mensagem = ""
    @Published var checkinEfetuado = false
    
    func doCheckin(_ completionFunction: @escaping (Bool) -> Void) {
        if(checkEmail()){
            AF.request(
                url,
                method: .post,
                parameters: eventCheckin,
                encoder: JSONParameterEncoder.default
            )
            .validate(statusCode: 200..<300)
            .response { response in
                switch response.result {
                    case .success:
                        self.falha = false
                        self.mensagem = "Seu checkin foi confirmado! :)"
                        completionFunction(true)
                    case .failure:
                        self.falha = true
                        if(response.response?.statusCode == 404){
                            self.mensagem = "O checkin não está disponível no momento. Tente novamente + tarde :("
                        }else{
                            self.mensagem = "Ocorreu um problema ao realizar o checkin. Tente novamente. Código do erro:  \(response.response!.statusCode)"
                        }
                    }
            }
        }
    }
    
    func checkEmail() -> Bool{
        if(self.eventCheckin.email.isEmpty){
            self.falha = true
            self.mensagem = "E-mail não pode ficar vazio!"
            return false
        }
        if self.eventCheckin.email.count > 100 {
            return false
        }
        let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        if(!emailPredicate.evaluate(with: self.eventCheckin.email)){
            self.falha = true
            self.mensagem = "O e-mail não tem um formato válido"
            return false
        }
        return true
    }
}

