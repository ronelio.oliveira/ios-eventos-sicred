//
//  EventDetail.swift
//  Eventos
//
//  Created by ronelio on 31/03/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct EventDetail: View {
    @State var showingSheet = false
    var event: Event
    var body: some View {
        List{
            ZStack(alignment: .bottom) {
                WebImage(url: URL(string: event.image))
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                Rectangle()
                    .frame(height: 80)
                    .opacity(0.40)
                    .blur(radius: 10)
                HStack{
                    VStack(alignment: .leading, spacing: 8){
                        Text(event.title)
                            .foregroundColor(.white)
                            .font(.largeTitle)
                    }
                    .padding(.leading)
                    .padding(.bottom)
                }
                Spacer()
            }
            .listRowInsets(EdgeInsets())
            Text(event.description)
                .foregroundColor(.primary)
                .font(.body)
                .lineLimit(nil)
                .lineSpacing(12)
            HStack {
                Spacer()
                Button(action: {self.showingSheet = true}){
                    Text("Presença!")
                }
                .frame(width: 200, height: 50)
                .foregroundColor(.white)
                .font(.headline)
                .background(Color.blue)
                .cornerRadius(10)
                .sheet(isPresented: $showingSheet) {
                    EventCheckinView(event: event)
                }
                Spacer()
            }.padding(.top, 50)
        }.edgesIgnoringSafeArea(.all)
    }
}

#if DEBUG
struct EventDetail_Previews: PreviewProvider {
    static var previews: some View {
        EventDetail(event: Event.init(date: 1534784400, description: "O Patas Dadas estará na Redenção, nesse domingo, com cães para adoção e produtos à venda!\n\nNa ocasião, teremos bottons, bloquinhos e camisetas!\n\nTraga seu Pet, os amigos e o chima, e venha aproveitar esse dia de sol com a gente e com alguns de nossos peludinhos - que estarão prontinhos para ganhar o ♥ de um humano bem legal pra chamar de seu. \n\nAceitaremos todos os tipos de doação:\n- guias e coleiras em bom estado\n- ração (as que mais precisamos no momento são sênior e filhote)\n- roupinhas \n- cobertas \n- remédios dentro do prazo de validade", image: "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png", longitude: -51.2146267, latitude: -30.0392981, price: 29.99, title: "Feira de adoção de animais na Redenção", id: "1"))
    }
}
#endif
