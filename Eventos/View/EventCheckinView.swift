//
//  EventCheckinView.swift
//  Eventos
//
//  Created by ronelio on 31/03/21.
//

import SwiftUI
import MapKit
import Combine

struct EventCheckinView: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var showingAlert = false
    @State private var messageValidation = ""
    @ObservedObject var eventCheckinViewModel: EventCheckinViewModel = EventCheckinViewModel()
    @State var region: MKCoordinateRegion

    var event: Event
    
    init(event: Event){
        self.event = event
        _region = State(initialValue: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: self.event.latitude, longitude: self.event.longitude), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)))
    }

    func checkinCallback(success: Bool) -> Void{
        if(success){
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    var body: some View {
        
        List{
            VStack(alignment: .leading) {
                Text("Checkin: ")
                    .font(.title)
                HStack{
                    Spacer()
                    
                    Text(event.title)
                        .font(.headline)
                    Spacer()
                }
                Spacer()
                eventMapView(region: region, event: self.event)
                Text("Nome:")
                    .font(.callout)
                    .bold()
                TextField("Qual seu nome?", text: $eventCheckinViewModel.eventCheckin.name)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                Text("Email:")
                    .font(.callout)
                    .bold()
                TextField("exemplo@email.com", text: $eventCheckinViewModel.eventCheckin.email)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
            }.padding()
            HStack {
                Spacer()
                Button("Confirmar Presença", action: {
                    if(eventCheckinViewModel.eventCheckin.name.isEmpty){
                        showingAlert = true
                        messageValidation = "Preencha seu nome!"
                        return
                    }
                    if(eventCheckinViewModel.eventCheckin.email.isEmpty){
                        showingAlert = true
                        messageValidation = "Preencha o e-mail"
                        return
                    }
                    eventCheckinViewModel.eventCheckin.eventId = self.event.id
                    eventCheckinViewModel.doCheckin(checkinCallback)
                })
                .frame(width: 200, height: 50)
                .foregroundColor(.white)
                .font(.headline)
                .background(Color.blue)
                .cornerRadius(10)
                .alert(isPresented: $eventCheckinViewModel.falha) {
                    Alert(title: Text("Erro no checkin!"), message: Text(eventCheckinViewModel.mensagem), dismissButton: .default(Text("OK")))
                }
                Spacer()
            }.padding(.top, 20)
        }.edgesIgnoringSafeArea(.all)
        .navigationBarTitle("Eventos")
        .alert(isPresented: $showingAlert) {
          Alert(title: Text("Atenção!"), message: Text(messageValidation), dismissButton: .default(Text("OK")))
        }
    }
}

struct eventMapView: View{
    @State var region: MKCoordinateRegion = MKCoordinateRegion()
    @State var event: Event
    var markers: [Marker] = []
    
    init(region: MKCoordinateRegion, event: Event){
        markers = [Marker(location: MapMarker(coordinate: CLLocationCoordinate2D(latitude: event.latitude, longitude: event.longitude), tint: .blue))]
        _region = State(initialValue: region)
        _event = State(initialValue: event)
    }

    var body: some View {
        Map(coordinateRegion: $region, showsUserLocation: false, userTrackingMode: .constant(.follow), annotationItems: markers){ marker in
            marker.location
        }
        .cornerRadius(10)
        .frame(height: 300)
        .edgesIgnoringSafeArea(.all)
    }
    
}

struct Marker: Identifiable {
    let id = UUID()
    var location: MapMarker
}

#if DEBUG
struct EventCheckinView_Previews: PreviewProvider {
    static var previews: some View {
        EventCheckinView(event: Event.init(date: 1534784400, description: "O Patas Dadas estará na Redenção, nesse domingo, com cães para adoção e produtos à venda!\n\nNa ocasião, teremos bottons, bloquinhos e camisetas!\n\nTraga seu Pet, os amigos e o chima, e venha aproveitar esse dia de sol com a gente e com alguns de nossos peludinhos - que estarão prontinhos para ganhar o ♥ de um humano bem legal pra chamar de seu. \n\nAceitaremos todos os tipos de doação:\n- guias e coleiras em bom estado\n- ração (as que mais precisamos no momento são sênior e filhote)\n- roupinhas \n- cobertas \n- remédios dentro do prazo de validade", image: "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png", longitude: -51.2146267, latitude: -30.0392981, price: 29.99, title: "Feira de adoção de animais na Redenção", id: "1"))
    }
}
#endif


