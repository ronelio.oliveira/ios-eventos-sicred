//
//  EventView.swift
//  Eventos
//
//  Created by ronelio on 30/03/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct EventView: View {
    let event:Event
    var body: some View {
        VStack(alignment: .leading, spacing: 16.0){
            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/) {
                HStack(alignment: .center) {
                    WebImage(url: URL(string: event.image))
                        .resizable()
                        .renderingMode(.original)
                        .placeholder {
                            Rectangle().foregroundColor(.gray).cornerRadius(10)
                        }
                        .indicator(.activity)
                        .transition(.fade(duration: 0.5))
                        .scaledToFit()
                        .cornerRadius(10)
                        .padding()        
                }
            }
            VStack{
                Text(event.title)
                    .foregroundColor(.primary)
                    .font(.headline)
                Text(event.description)
                    .foregroundColor(.secondary)
                    .font(.subheadline)
                    .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                    .lineLimit(2)
                    .frame(height: 40)
            }
            .padding(.leading, 10)
            .padding(.trailing, 10)
        }
    }
}

#if DEBUG
struct EventView_Previews: PreviewProvider {
    static var previews: some View {
        EventView(event: Event.init(date: 1534784400, description: "O Patas Dadas estará na Redenção, nesse domingo, com cães para adoção e produtos à venda!\n\nNa ocasião, teremos bottons, bloquinhos e camisetas!\n\nTraga seu Pet, os amigos e o chima, e venha aproveitar esse dia de sol com a gente e com alguns de nossos peludinhos - que estarão prontinhos para ganhar o ♥ de um humano bem legal pra chamar de seu. \n\nAceitaremos todos os tipos de doação:\n- guias e coleiras em bom estado\n- ração (as que mais precisamos no momento são sênior e filhote)\n- roupinhas \n- cobertas \n- remédios dentro do prazo de validade", image: "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png", longitude: -51.2146267, latitude: -30.0392981, price: 29.99, title: "Feira de adoção de animais na Redenção", id: "1"))
    }
}
#endif
