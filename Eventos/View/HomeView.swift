//
//  ContentView.swift
//  Eventos
//
//  Created by ronelio on 30/03/21.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var viewModel = EventViewModel()

    var body: some View {
        if viewModel.events.count <= 0{
            HStack {
                Spacer()
                VStack(alignment: .center) {
                    Text("Eventos")
                        .font(.system(size: 30))
                    Spacer()
                    ProgressView("Aguarde, buscando eventos...").onAppear{
                        self.viewModel.fetchEvents()
                    }
                    Spacer()
                }
                Spacer()
            }
        }else{
            NavigationView {
                    List(viewModel.events, id: \.self) { event in
                        NavigationLink(destination: EventDetail(event: event)){
                            EventView(event: event).frame(
                                alignment: .leading)
                        }
                    }
                    .navigationBarTitle("Eventos")
                    .onAppear {
                        self.viewModel.fetchEvents()
                    }
            }
        }
    }
}

struct OrangeLine: View {
    var body: some View {
        Rectangle()
            .fill(Color.orange)
            .frame(height: 2)
            .edgesIgnoringSafeArea(.horizontal)
    }
}

#if DEBUG
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
#endif



