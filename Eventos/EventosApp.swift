//
//  EventosApp.swift
//  Eventos
//
//  Created by ronelio on 30/03/21.
//

import SwiftUI

@main
struct EventosApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
