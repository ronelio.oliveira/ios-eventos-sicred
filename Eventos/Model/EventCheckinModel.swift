//
//  EventCheckinModel.swift
//  Eventos
//
//  Created by ronelio on 31/03/21.
//

import SwiftUI

struct EventCheckin: Hashable, Encodable, Decodable{
    var eventId: String
    var name: String
    var email: String
}
