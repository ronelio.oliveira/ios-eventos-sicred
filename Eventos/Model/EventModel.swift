//
//  EventModel.swift
//  Eventos
//
//  Created by ronelio on 30/03/21.
//

import SwiftUI

struct Event: Hashable, Decodable{
    let date: Int
    let description: String
    let image: String
    let longitude: Double
    let latitude: Double
    let price: Double
    let title: String
    let id: String
}
